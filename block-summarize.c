/*-
 * Copyright (c) 2010  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#define _GNU_SOURCE

#include <ctype.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BLKSIZE_DEFAULT	32

static int	 get_number(FILE *fp, size_t *num, char eol);
static void	 finish_block(size_t blk, size_t current);
static void	 finish_all(size_t total);

int
main(int argc, char * const argv[])
{
	int any, ch;
	char *end;
	size_t blksize, blk, nblk, num, current, total;

	blksize = BLKSIZE_DEFAULT;
	while ((ch = getopt(argc, argv, "b:")) != -1)
		switch (ch) {
			case 'b':
				blksize = strtoul(optarg, &end, 10);
				if (end == optarg || *end != '\0')
					errx(1, "Invalid blocksize %s",
					    optarg);
				break;

			default:
				errx(1, "Invalid command-line option %c\n",
				    ch);
				break;
		}

	/* TODO: read from a file */

	any = 0;
	blk = current = total = 0;
	while (get_number(stdin, &num, '\n')) {
		nblk = num / blksize;
		if ((nblk != blk) || !any) {
			if (any)
				finish_block(blk, current);
			else
				any = 1;
			blk = nblk;
			current = 0;
		}
		current++;
		total++;
	}
	if (any) {
		finish_block(blk, current);
		finish_all(total);
	}
	return (0);
}

/*
 * Function:
 *	get_number	- read a number at the start of a line
 * Inputs:
 *	fp		- the stream to read the number from
 *	num		- (val/res) the number read
 *	eol		- the end-of-line character
 * Returns:
 *	True if a number has been read, false on EOF or a non-number line.
 * Modifies:
 *	Reads from stdin.
 */
static int
get_number(FILE *fp, size_t *num, char eol)
{
	int c;
	size_t val;

	do {
		c = getc(fp);
	} while (c != EOF && c != eol && isspace(c));
	if (c == EOF || c == eol || !isdigit(c))
		return 0;

	val = c - '0';
	/* Read the number */
	while ((c = getc(fp)) != EOF && isdigit(c))
		val = val * 10 + (c - '0');

	/* Skip to EOL or EOF */
	while (c != EOF && c != eol)
		c = getc(fp);
	*num = val;
	return 1;
}

/*
 * Function:
 *	finish_block	- display statistics about a single block
 * Inputs:
 *	blk		- the block number
 *	current		- the number of differences in this block
 * Returns:
 *	Nothing.
 * Modifies:
 *	Writes to the standard output.
 */
static void
finish_block(size_t blk, size_t current)
{
	printf("block\t%lu\t%lu diff%s\n", (unsigned long)blk,
	    (unsigned long)current, current == 1? "": "s");
}

/*
 * Function:
 *	finish_total	- display final statistics
 * Inputs:
 *	total		- the total number of differences
 * Returns:
 *	Nothing.
 * Modifies:
 *	Writes to the standard output.
 */
static void
finish_all(size_t total)
{
	printf("total\t%lu\n", (unsigned long)total);
}
